from main.database import db
import datetime
import uuid
import json
from flask import request
from sqlalchemy.types import TypeDecorator, CHAR


class GUID(TypeDecorator):
    """Platform-independent GUID type.

    Uses PostgreSQL's UUID type, otherwise uses
    CHAR(32), storing as stringified hex values.

    """

    impl = CHAR

    def load_dialect_impl(self, dialect):
        if dialect.name == "postgresql":
            return dialect.type_descriptor(UUID())
        else:
            return dialect.type_descriptor(CHAR(32))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == "postgresql":
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value).int
            else:
                # hexstring
                return "%.32x" % value.int

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            if not isinstance(value, uuid.UUID):
                value = uuid.UUID(value)
            return value


class FrameworkBaseModel(db.Model):
    __abstract__ = True
    id = db.Column(
        GUID(), primary_key=True, default=uuid.uuid4, unique=True, nullable=False
    )
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated_at = db.Column(
        db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.utcnow
    )


class FrameworkChangelog(FrameworkBaseModel):
    __tablename__ = "framework_changelog"
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.Text)

class UserTables(FrameworkBaseModel):
    __tablename__ = "Users"
    name =db.Column(db.String(100))
    age = db.Column(db.Integer)

    # def json(self):
    #     return {'id': self.id, 'created_at': self.created_at,
    #             'updated_at': self.updated_at, 'name': self.name,'age':self.age}

    def add_user(name,age):
        new_user = UserTables(name=name, age=age)
        db.session.add(new_user)
        db.session.commit()

    @classmethod
    def get_list(cls):
        records=cls.query.all()
        return records
    @classmethod
    def edit_details(cls,id,key,data):

        record = cls.query.filter_by(id=id).first();

        if(key=='name'):

            record.name = data
            record.updated_at = datetime.datetime.utcnow();
            db.session.commit()
            return record

        elif(key=='age'):

            record.age = data
            record.updated_at = datetime.datetime.utcnow()
            db.session.commit()
            return record


    @classmethod
    def find_by_id(cls, id):
        try:
            return cls.query.filter_by(id=id).first();
        except Exception as e:
            print(e);

    def delete_from_db(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception as e:
            print(e)








