from flask import Flask, request
from flask import render_template


def index():
    return render_template("index.html")
