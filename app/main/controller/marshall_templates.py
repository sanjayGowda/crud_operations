from flask_restful import fields, marshal_with

from main.helpers.constants import pagination_fields
from main.helpers.constants import default_per_page, default_page

changelog_fields = {
    "id": fields.String,
    "name": fields.String,
    "description": fields.String,
    "created_at": fields.DateTime(dt_format="iso8601"),
    "updated_at": fields.DateTime(dt_format="iso8601"),
}

# fields.DateTime(dt_format='iso8601')

framework_changelog_fields = {
    "changelog": fields.List(fields.Nested(changelog_fields)),
    "pagination": fields.Nested(pagination_fields),
}


framework_job_fields = {
    "name": fields.String,
    "id": fields.String,
}
users_fields={
    "id": fields.String,
    "name":fields.String,
    "age":fields.Integer
}

