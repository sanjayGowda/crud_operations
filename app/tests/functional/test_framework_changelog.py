from main import models


def test_empty_changelog(client, init_database):
    response = client.get("/api/changelog")
    assert response.status_code == 200
    assert len(response.json["changelog"]) == 0


def test_once_changelog(client, init_database):
    # insert one changelog and see if it returns
    changelog = models.FrameworkChangelog()
    changelog.name = "v.test"
    changelog.description = "this is testing"
    init_database.session.add(changelog)
    init_database.session.commit()
    response = client.get("/api/changelog")
    assert response.status_code == 200
    assert len(response.json["changelog"]) > 0
    assert response.json["changelog"][0]["name"] == "v.test"

