import os

basedir = os.path.abspath(os.path.dirname(__file__))


def getConfig(flask_env):
    print(
        "\n========================== SETTING ENV START=============================================="
    )
    config = config_by_name[flask_env]
    print("config", config)
    print(
        "========================== SETTING ENV END=============================================="
    )
    return config


class Config:
    # SQLALCHEMY_POOL_RECYCLE = 3600
    DEBUG = False


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CELERY_BROKER_URL = "redis://localhost:6379"
    CELERY_RESULT_BACKEND = "redis://localhost:6379"


class LocalConfig(Config):
    if os.environ["FLASK_ENV"] == "local":
        DEBUG = True
        TEMP_DIR = os.path.join(basedir, "./temp")
        SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(TEMP_DIR, "test.db")
        SQLALCHEMY_TRACK_MODIFICATIONS = False
        CELERY_BROKER_URL = "redis://localhost:6379"
        CELERY_RESULT_BACKEND = "redis://localhost:6379"


class DevelopmentConfig(Config):
    if os.environ["FLASK_ENV"] == "development":
        DEBUG = False
        SQLALCHEMY_TRACK_MODIFICATIONS = False
        SQLALCHEMY_DATABASE_URI = os.environ["SQLALCHEMY_DATABASE_URI"]
        CELERY_BROKER_URL = os.environ["CELERY_BROKER_URL"]
        CELERY_RESULT_BACKEND = os.environ["CELERY_RESULT_BACKEND"]


class MasterConfig(Config):
    if os.environ["FLASK_ENV"] == "master":
        DEBUG = False
        SQLALCHEMY_TRACK_MODIFICATIONS = False
        SQLALCHEMY_DATABASE_URI = os.environ["SQLALCHEMY_DATABASE_URI"]
        CELERY_BROKER_URL = os.environ["CELERY_BROKER_URL"]
        CELERY_RESULT_BACKEND = os.environ["CELERY_RESULT_BACKEND"]


class ProductionConfig(Config):
    if os.environ["FLASK_ENV"] == "production":
        DEBUG = False
        SQLALCHEMY_TRACK_MODIFICATIONS = False
        SQLALCHEMY_DATABASE_URI = os.environ["SQLALCHEMY_DATABASE_URI"]
        CELERY_BROKER_URL = os.environ["CELERY_BROKER_URL"]
        CELERY_RESULT_BACKEND = os.environ["CELERY_RESULT_BACKEND"]


config_by_name = dict(
    local=LocalConfig,
    development=DevelopmentConfig,
    master=MasterConfig,
    production=ProductionConfig,
    testing=TestingConfig,
)
