import os
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from main import models
from application import create_app
from main.database import db


if __name__ == "__main__":
    if "FLASK_ENV" in os.environ:
        environment = os.environ["FLASK_ENV"]
        app, api, celery = create_app(environment)
        manager = Manager(app)
        migrate = Migrate(app, db)
        with app.app_context():
            if db.engine.url.drivername == "sqlite":
                migrate.init_app(app, db, render_as_batch=True)
            else:
                migrate.init_app(app, db)
        manager.add_command("db", MigrateCommand)
        manager.run()
    else:
        print("No FLASK_ENV found")
