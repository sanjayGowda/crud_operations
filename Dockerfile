FROM docker.io/python:3.6-buster

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev


COPY ./app /app

WORKDIR /app

RUN pip install -r requirements.txt

CMD ["gunicorn", "--bind", ":8000", "--workers", "1", "application:app"]