# Setup and Run Locally
## Create a virtual env

`python3 -m venv ~/flask-micro-service`

## Use the venv

`source ~/flask-micro-service/bin/activate`

## install the packages

`pip3 install -r requirements.txt`

## Edit config if you like to

`nano config.py`

## Set FLASK_ENV variable

`export FLASK_ENV=local`

## Create an initial localdb
`python manage.py db upgrade`

This will create a test.db inside the temp forder for testing. Its an sqlite db. This command also create the tables.

### Tip

Run this `python manage.py`, it will show all the available admin commands. We can add more


## Start the server

`python application.py`

## Test

`http://localhost:5000`



# Start the celery job worker

## Setup env
`export FLASK_ENV=local`

## Start redis server
So its availabe on local machine or you can change in config

## Start celery worker
`celery -A application.celery worker -l info`


## Try
- Start web app
- Visit `http://localhost:5000/api/job`
- See the worker console

# How to add new column or table 

## Edit Models

- Edit main/models.py
- Add or Edit the model
- Save it

## Generate a new  migration
- `python manage.py db migrate -m "Added new table"`
- This will create a new  file whith all the SQLs required for the changes
- Edit if you like to

## Then update the database
- `python manage.py db upgrade`
- This should update the configured database

## Commit the migration files
- Its important to commit and push the migration files
- So it can be used by others
- Also for later deployment to production


# Testing
## Setup env
`export FLASK_ENV=development`

## Run pytest
`cd app`
`pytest -v`

## Single step
`export FLASK_ENV=testing;pytest`

# Deployment to EBS
1. Change the code
2. Test locally, if everything is fine
3. Make sure your EBS env settings has  `PRODUCTION_SQLALCHEMY_DATABASE_URI=` and `FLASK_ENV=production` set
4. PRODUCTION_SQLALCHEMY_DATABASE_URI will look like
	`PRODUCTION_SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://(user):(password)@(db_identifier).amazonaws.com:3306/(db_name)`
5. Go into app folder
6. Zip all the files inside it, call it say upload.zip or eb deploy?


# Deployment to Gunicorn
1. You can start using `gunicorn -w 4 application:app`



# Using Docker or Podman

## Build
`docker build -t flask-micro-service:latest .`


## Run and then remove once stopped
`docker run  --rm --name flask-micro-service-development --env FLASK_ENV=development -p 8000:8000 flask-micro-service`



